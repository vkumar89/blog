#assignment 2
class Post < Activerecord::base
 has_many :comments, dependent: :destroy
 validates_presence_of :title
 validates_presence_of :body
end
